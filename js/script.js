

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    var pm1 = "scene/1m_Lit.jpg";
    var pm2 = "scene/2m_Lit.jpg";
    var pm3 = "scene/3m_Lit.jpg";
    var pm4 = "scene/4m_Lit.jpg";
    var pm5 = "scene/5m_Lit.jpg";
    var pm6 = "scene/6m_Lit.jpg";
    var pm7 = "scene/7m_Lit.jpg";
    var pm8 = "scene/8m_Lit.jpg";
}else{
    var pm1 = "scene/1.jpg";
    var pm2 = "scene/2.jpg";
    var pm3 = "scene/3.jpg";
    var pm4 = "scene/4.jpg";
    var pm5 = "scene/5.jpg";
    var pm6 = "scene/6.jpg";
    var pm7 = "scene/7.jpg";
    var pm8 = "scene/8.jpg";
    $('.responsive-img').loupe({
		width: 300, // ширина лупы
		height: 250, // высота лупы
		loupe: 'loupe' // установить css класс лупы - для курсора и тд
	});
}
function getName(iid) {
	var itm = jQuery('#popup'+iid);
	if (itm.length) {
		return itm.find('h4').text().slice(1,-1);
	}
	return 'Picture ' + iid;
}
function getName2(iid) {
	var itm = jQuery('#popup'+iid);
	if (itm.length) {
		return itm.find('h5').text().slice(1,-1);
	}
	return 'Picture ' + iid;
}
viewer = pannellum.viewer('panorama', {
    "showControls": false,
    "default": {
        "firstScene": "s1",
        "sceneFadeDuration": 1000,
        "compass": true,
        "autoLoad": true
    },
    "scenes": {
        "s1": {
            "title": "Зал 1",
            "hfov": 110,
            "pitch": -5,
            "yaw": 117,
            "type": "multires",
            "multiRes": {                          
                   "path": "scene/1/%l/%s%y_%x",             
                   "fallbackPath": "scene/1/fallback/%s",    
                   "extension": "jpg",                
                   "tileResolution": 512,             
                   "maxLevel": 4,                     
                   "cubeResolution": 2224             
               },            
            "autoLoad": true,
            "hotSpots": [
                {
                    "pitch": 22,
                    "yaw": 75,
                    "cssClass": "custom-hotspot",
                    "text": getName(1),
                    "clickHandlerFunc": popup,
                    "clickHandlerArgs": "popup1"
                },{
                    "pitch": 20,
                    "yaw": 255,
                    "cssClass": "custom-hotspot",
                    "text": getName2('1-1'),
                    "clickHandlerFunc": popup,
                    "clickHandlerArgs": "popup1-1"
                },{
                    "pitch": 15,
                    "yaw": 165,
                    "cssClass": "custom-hotspot",
                    "text": getName2('1-2'),
                    "clickHandlerFunc": popup,
                    "clickHandlerArgs": "popup1-2"
                },{
                    "pitch": 25,
                    "yaw": -29.5,
                    "cssClass": "custom-hotspot",
                    "text": getName2('1-3'),
                    "clickHandlerFunc": popup,
                    "clickHandlerArgs": "popup1-3"
                },{
                    "pitch": 7,
                    "yaw": -16.5,
                    "cssClass": "custom-hotspot",
                    "text": getName2('1-4'),
                    "clickHandlerFunc": popup,
                    "clickHandlerArgs": "popup1-4"
                },{
                    "pitch": -10,
                    "yaw": 215,
                    "cssClass": "arrow-right",
                    "text": "Зал 7",
                    "sceneId": "s7"
                }, {
                    "pitch": -10,
                    "yaw": 25,
                    "cssClass": "arrow-left",
                    "text": "Зал 5",
                    "sceneId": "s5"
                }, {
                    "pitch": -10,
                    "yaw": -215,
                    "cssClass": "arrow-left",
                    "text": "Зал 8",
                    "sceneId": "s8"
                }
            ]
        },
        "s2": {
            "title": "Зал 2",
            "hfov": 100,
            "yaw": 5,
            "type": "multires",
            "multiRes": {                          
                   "path": "scene/2/%l/%s%y_%x",             
                   "fallbackPath": "scene/2/fallback/%s",    
                   "extension": "jpg",                
                   "tileResolution": 512,             
                   "maxLevel": 4,                     
                   "cubeResolution": 2224             
               },           
            "autoLoad": true,
            "hotSpots": [{
                "pitch": 13,
                "yaw": 4,
                "cssClass": "custom-hotspot",
                "text": getName(2),
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup2"
            }, {
                "pitch": 6,
                "yaw": 39.5,
                "cssClass": "custom-hotspot",
                "text": getName(3),
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup3"
            }, {
                "pitch": 6,
                "yaw": 49.5,
                "cssClass": "custom-hotspot",
                "text": getName('2-2'), //"Иван Айвазовский. К 200-летию со дня рождения"
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup2-2"
            },{
                
                "pitch": 5,
                "yaw": 256.5,
                "cssClass": "custom-hotspot",
                "text": getName2('2-1'), //"«Лунная ночь. Неаполь»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup2-1"
                
            },{
                "pitch": 2,
                "yaw": 276.5,
                "cssClass": "custom-hotspot",
                "text": getName2('6-2'), //"«Бурное море»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup6-2"
            },{
                "pitch": 3,
                "yaw": 279.5,
                "cssClass": "custom-hotspot",
                "text": getName2('6-3'), //"«Буря»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup6-3"
            },{
                "pitch": 2,
                "yaw": 283,
                "cssClass": "custom-hotspot",
               "text": getName2('6-4'), //"«Буря на море»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup6-4"
            },{
                "pitch": -10,
                "yaw": 275,
                "cssClass": "arrow-left",
                "text": "Зал 6",
                "sceneId": "s6"
            }, {
                "pitch": -10,
                "yaw": -30,
                "cssClass": "arrow-left",
                "text": "Зал 7",
                "sceneId": "s5"
            }, {
                "pitch": -10,
                "yaw": 25,
                "cssClass": "arrow-right",
                "text": "Зал 8",
                "sceneId": "s8"
            }]
        },
        "s3": {
            "title": "Зал 3",
            "hfov": 100,
            "yaw": 5,
            "type": "multires",
            "multiRes": {                          
                   "path": "scene/3/%l/%s%y_%x",             
                   "fallbackPath": "scene/3/fallback/%s",    
                   "extension": "jpg",                
                   "tileResolution": 512,             
                   "maxLevel": 4,                     
                   "cubeResolution": 2224             
               },            
            "autoLoad": true,
            "hotSpots": [{
                "pitch": 1,
                "yaw": 56.5,
                "cssClass": "custom-hotspot",
                "text": getName(4),
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup4"
            },{
                "pitch": 10,
                "yaw": -111,
                "cssClass": "custom-hotspot",
                "text": getName2('3-1'), //"«Отцы-мхитаристы на острове Святого Лазаря. Венеция»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup3-1"
            },{
                "pitch": 10,
                "yaw": -90,
                "cssClass": "custom-hotspot",
                "text": getName2('3-2'), //"Портрет Г. К. Айвазовского, брата художника.",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup3-2"
            },
            {
                "pitch": 8,
                "yaw": -74,
                "cssClass": "custom-hotspot",
                "text": getName2('3-3'), //"Портрет А.Н. Айвазовской, жены художника.",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup3-3"
            },{
                "pitch": -10,
                "yaw": 215,
                "cssClass": "arrow-right",
                "text": "Зал 6",
                "sceneId": "s6"
            }]
        },
        "s4": {
            "title": "Зал 4",
            "hfov": 100,
            "yaw": 5,
            "type": "multires",
            "multiRes": {                          
                   "path": "scene/4/%l/%s%y_%x",             
                   "fallbackPath": "scene/4/fallback/%s",    
                   "extension": "jpg",                
                   "tileResolution": 512,             
                   "maxLevel": 4,                     
                   "cubeResolution": 2224             
               },            
            "autoLoad": true,
            "hotSpots": [{
                "pitch": 5,
                "yaw": -73.5,
                "cssClass": "custom-hotspot",
                "text": getName(5),
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup5"
            }, {
                "pitch": -3,
                "yaw": -73.5,
                "cssClass": "custom-hotspot",
                "text": getName(6),
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup6"
            },{
                "pitch": 13,
                "yaw": 120,
                "cssClass": "custom-hotspot",
                "text": getName2('4-1'), //"«Вид Одессы в лунную ночь»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup4-1"
            },{
                "pitch": 13,
                "yaw": 156,
                "cssClass": "custom-hotspot",
                "text": getName2('4-2'), //"«Вид на Лаго-Маджоре и Изола-Белла в лунную ночь»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup4-2"
            },{
                "pitch": 13,
                "yaw": 192,
                "cssClass": "custom-hotspot",
                "text": getName2('4-3'), //"«Лунная ночь в Крыму»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup4-3"
            },{
                "pitch": 13,
                "yaw": 225,
                "cssClass": "custom-hotspot",
                "text": getName2('4-4'), //"«Гондольер на море ночью»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup4-4"
            }, {
                "pitch": -20,
                "yaw": 50,
                "type": "scene",
                "text": "Зал 5",
                "cssClass": "arrow-left",
                "sceneId": "s5"
            }]
        },
        "s5": {
            "title": "Зал 5",
            "hfov": 100,
            "yaw": 5,
            "type": "multires",
            "multiRes": {                          
                   "path": "scene/5/%l/%s%y_%x",             
                   "fallbackPath": "scene/5/fallback/%s",    
                   "extension": "jpg",                
                   "tileResolution": 512,             
                   "maxLevel": 4,                     
                   "cubeResolution": 2224             
               },            
            "autoLoad": true,
            "hotSpots": [{
                "pitch": 3,
                "yaw": -41.5,
                "cssClass": "custom-hotspot",
                "text": getName(7),
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup7"
            }, {
                "pitch": 3,
                "yaw": -35.3,
                "cssClass": "custom-hotspot",
                "text": getName(8),
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup8"
            }, {
                "pitch": 13,
                "yaw": 51,
                "cssClass": "custom-hotspot",
                "text": getName(9),
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup9"
            },{
                "pitch": 18,
                "yaw": 20,
                "cssClass": "custom-hotspot",
                "text": getName2('5-1'), //"«Вид Константинополя при лунном освещении»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup5-1"
            },{
                "pitch": 17,
                "yaw": 75,
                "cssClass": "custom-hotspot",
                "text": getName2('5-2'), //"«Лунная ночь на берегу моря в Крыму»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup5-2"
            },{
                "pitch": 15,
                "yaw": 192,
                "cssClass": "custom-hotspot",
                "text": getName2('5-3'), //"«Буря»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup5-3"
            },{
                "pitch": 35,
                "yaw": 225,
                "cssClass": "custom-hotspot",
                "text": getName2('5-4'), //"«Морской вид при луне»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup5-4"
            },{
                "pitch": 16.5,
                "yaw": 255.5,
                "cssClass": "custom-hotspot",
                "text": getName2('5-5'), //"«Корабли в бурном море. Восход солнца»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup5-5"
            },{
                "pitch": 8,
                "yaw": 112,
                "cssClass": "custom-hotspot",
                "text": getName2('5-10'), //"«Ледяные горы»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup5-10"
            },{
                "pitch": 10,
                "yaw": 145,
                "cssClass": "custom-hotspot",
                "text": getName2('5-11'), //"«Сотворение мира»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup5-11"
            },{
                "pitch": -5,
                "yaw": -63,
                "cssClass": "arrow-left",
                "text": "Зал 4",
                "sceneId": "s4"
            }, {
                "pitch": -10,
                "yaw": -5,
                "cssClass": "arrow-right",
                "text": "Зал 8",
                "sceneId": "s8"
            }, {
                "pitch": -10,
                "yaw": 95,
                "cssClass": "arrow-left",
                "text": "Зал 1",
                "sceneId": "s1"
            }, {
                "pitch": -8,
                "yaw": -203,
                "cssClass": "arrow-right",
                "text": "Зал 4",
                "sceneId": "s4"
            }]
        },
        "s6": {
            "title": "Зал 6",
            "hfov": 100,
            "yaw": 5,
            "type": "multires",
            "multiRes": {                          
                   "path": "scene/6/%l/%s%y_%x",             
                   "fallbackPath": "scene/6/fallback/%s",    
                   "extension": "jpg",                
                   "tileResolution": 512,             
                   "maxLevel": 4,                     
                   "cubeResolution": 2224             
               },            
            "autoLoad": true,
            "hotSpots": [{
                "pitch": 23,
                "yaw": 145,
                "cssClass": "custom-hotspot",
                "text": getName(10),
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup10"
            },{
                "pitch": 12.5,
                "yaw": 79,
                "cssClass": "custom-hotspot",
                "text": getName2('6-1'), //"«Лунная ночь. Неаполь»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup6-1"
            },{
                "pitch": 15,
                "yaw": 275,
                "cssClass": "custom-hotspot",
                "text": getName2('6-2'), //"«Бурное море»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup6-2"
            },{
                "pitch": 18,
                "yaw": 310,
                "cssClass": "custom-hotspot",
                "text": getName2('6-3'), //"«Буря»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup6-3"
            },{
                "pitch": 10,
                "yaw": 331,
                "cssClass": "custom-hotspot",
               "text": getName2('6-4'), //"«Буря на море»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup6-4"
            },{
                "pitch": 14,
                "yaw": 200,
                "cssClass": "custom-hotspot",
                "text": getName2('6-5'), //"«Облака над морем. Штиль»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup6-5"
            },{
                "pitch": -5,
                "yaw": -125,
                "cssClass": "arrow-right",
                "text": "Зал 3",
                "sceneId": "s3"
            }, {
                "pitch": -10,
                "yaw": -5,
                "cssClass": "arrow-left",
                "text": "Зал 7",
                "sceneId": "s7"
            }, {
                "pitch": -10,
                "yaw": 50,
                "cssClass": "arrow-right",
                "text": "Зал 2",
                "sceneId": "s2"
            }]
        },
        "s7": {
            "title": "Зал 7",
            "hfov": 100,
            "yaw": 5,
            "type": "multires",
            "multiRes": {                          
                   "path": "scene/7/%l/%s%y_%x",             
                   "fallbackPath": "scene/7/fallback/%s",    
                   "extension": "jpg",                
                   "tileResolution": 512,             
                   "maxLevel": 4,                     
                   "cubeResolution": 2224             
               },            
            "autoLoad": true,
            "hotSpots": [{
                "pitch": 4.5,
                "yaw": 151,
                "cssClass": "custom-hotspot",
                "text": "Украинский пейзаж с чумаками при луне",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup11"
            },{
                "pitch": 30,
                "yaw": 280,
                "cssClass": "custom-hotspot",
                "text": getName2('7-1'), //"«Девятый вал»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup7-1"
            },{
                "pitch": 13,
                "yaw": 85,
                "cssClass": "custom-hotspot",
                "text": getName2('7-2'), //"«Залив в Феодосии. Восход солнца»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup7-2"
            },{
                "pitch": 14,
                "yaw": 114,
                "cssClass": "custom-hotspot",
                "text": getName2('7-3'), //"«Гурзуф ночью»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup7-3"
            },{
                "pitch": 10,
                "yaw": 130,
                "cssClass": "custom-hotspot",
                "text": getName2('7-4'), //"«Вид Феодосии. Закат»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup7-4"
            },{
                "pitch": 13,
                "yaw": 43,
                "cssClass": "custom-hotspot",
                "text": getName2('7-5'), //"«Буря у мыса Айя»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup7-5"
            },{
                "pitch": 9,
                "yaw": 225,
                "cssClass": "custom-hotspot",
                "text": getName2('7-6'), //"«Синопский бой»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup7-6"
            },{
                "pitch": -10,
                "yaw": -140,
                "cssClass": "arrow-left",
                "text": "Зал 1",
                "sceneId": "s1"
            }, {
                "pitch": -10,
                "yaw": -45,
                "cssClass": "arrow-right",
                "text": "Зал 8",
                "sceneId": "s8"
            }, {
                "pitch": -5,
                "yaw": 28,
                "cssClass": "arrow-left",
                "text": "Зал 2",
                "sceneId": "s2"
            }, {
                "pitch": -5,
                "yaw": 140,
                "cssClass": "arrow-right",
                "text": "Зал 6",
                "sceneId": "s6"
            }]
        },
        "s8": {
            "title": "Зал 8",
            "hfov": 100,
            "yaw": 5,
            "type": "multires",
            "multiRes": {                          
                   "path": "scene/8/%l/%s%y_%x",             
                   "fallbackPath": "scene/8/fallback/%s",    
                   "extension": "jpg",                
                   "tileResolution": 512,             
                   "maxLevel": 4,                     
                   "cubeResolution": 2224             
               },            
            "autoLoad": true,
            "hotSpots": [{
                "pitch": 5,
                "yaw": 134,
                "cssClass": "custom-hotspot",
                "text": getName(12),
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup12"
            },{
                "pitch": 31,
                "yaw": -60,
                "cssClass": "custom-hotspot",
                "text": getName2('8-1'), //"«Волна»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup8-1"
            },{
                "pitch": 20,
                "yaw": -132,
                "cssClass": "custom-hotspot",
                "text": getName2('8-2'), //"«Бриг “Меркурий” после победы над двумя турецкими судами встречается с русской эскадрой»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup8-2"
            },{
                "pitch": 9,
                "yaw": 170,
                "cssClass": "custom-hotspot",
                "text": getName2('8-3'), //"«Буря у мыса Айя»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup8-3"
            },{
                "pitch": 13,
                "yaw": 100,
                "cssClass": "custom-hotspot",
                "text": getName2('8-4'), //"«Вид на Москву с Воробьевых гор»",
                "clickHandlerFunc": popup,
                "clickHandlerArgs": "popup8-4"
            },{
                "pitch": -10,
                "yaw": -100,
                "cssClass": "arrow-right",
                "text": "Зал 7",
                "sceneId": "s7"
            }, {
                "pitch": -20,
                "yaw": 134,
                "cssClass": "arrow-left",
                "text": "Зал 2",
                "sceneId": "s2"
            }]
        }
    }
});

viewer.on('scenechange', function (e) {
    //console.log(viewer.getScene());
})
setInterval(function(){
	var activeP = jQuery('.dots.active');
    var cury = viewer.getYaw();
    //console.log(cury);
    activeP.rotate(cury);
}, 100)
	$('#panorama').on('load', function(e){        
        var foo = jQuery('.pnlm-panorama-info'); 
        foo.detach();      
        foo.appendTo('.title');
    });
    
    viewer.on('error', function (e) {
        location.href='/error.html';
    })
    
    //var svgdom = $("#schemaMiniSVG")[0].contentDocument;
    var fId = viewer.getScene();
    //console.log(fId);
    jQuery(".dots").removeClass('active');
    jQuery(".dots.p" + fId).addClass("active");
  
    jQuery("body").on('click', '.btns', function () {
        if ($(this).data('id') == undefined){return;}
        var said = $(this).data('id');
        jQuery(".dots").removeClass('active');
    jQuery(".dots.p" + said).addClass("active");
        ga('send', 'event', 'view', 'gal', 'vt_prosm_pan_'+  said.slice(-1));
        viewer.loadScene(said);
        $('#schema').modal('close');
    })
    viewer.on('load', function (e) {
        
        var iid = viewer.getScene();
        jQuery(".dots").removeClass('active');
    jQuery(".dots.p" + iid).addClass("active");
        $('.panorama-info').text('Зал ' + iid.slice(-1));
    })

$('.min').click(function(e){
    if($('#mini-schema').data("t") == 0){
        $('.mapcont.small').slideToggle('slow', function(e){
            $('#mini-schema').text('Развернуть карту');
            $('#mini-schema').data("t", 1);
            $('#mini-schema').attr('href', '#')
        });
    }
    if($('#mini-schema').data("t") == 1){
        $('.mapcont.small').slideToggle('slow', function(e){
            $('#mini-schema').text('Увеличить схему');
            $('#mini-schema').attr('href', '#schema');
            $('#mini-schema').data("t", 0);
        })     
    }
})
$('#mini-schema').click(function(e){
    if($('#mini-schema').data("t") == 1){
        $('.mapcont.small').slideToggle('slow', function(e){
            $('#mini-schema').text('Увеличить схему');
            $('#mini-schema').attr('href', '#schema');
            $('#mini-schema').data("t", 0);
        })     
    }    
})
$('.slide a').click(function(e){
    e.preventDefault(); 
	ga('send', 'event', 'view', 'gal', 'vt_prosm_pan_'+  $(this).attr('id').slice(-1));
    viewer.loadScene($(this).attr('id')); 
   
});
/*
*/
document.getElementById('ctrl-bottom').addEventListener('click', function (e) {
    e.preventDefault();
    viewer.setPitch(viewer.getPitch() - 10);
});
document.getElementById('ctrl-top').addEventListener('click', function (e) {
    e.preventDefault();
    viewer.setPitch(viewer.getPitch() + 10);
});
document.getElementById('ctrl-left').addEventListener('click', function (e) {
    e.preventDefault();
    viewer.setYaw(viewer.getYaw() - 10);
});
document.getElementById('ctrl-right').addEventListener('click', function (e) {
    e.preventDefault();
    viewer.setYaw(viewer.getYaw() + 10);
});
document.getElementById('ctrl-zoom-in').addEventListener('click', function (e) {
    e.preventDefault();
    viewer.setHfov(viewer.getHfov() - 10);
});
document.getElementById('ctrl-zoom-out').addEventListener('click', function (e) {
    e.preventDefault();
    viewer.setHfov(viewer.getHfov() + 10);
});
document.getElementById('ctrl-fullscreen').addEventListener('click', function (e) {
    viewer.toggleFullscreen();
});
document.getElementById('ctrl-rotate').addEventListener('click', function (e) {
    e.preventDefault();
    viewer.setYaw(viewer.getYaw() - 180);
});
$('.modal').modal();
$('.play-scene').click(function(e){
	e.preventDefault();
	$('#start-screen').fadeOut();
	//viewer.loadScene("s1");
})
function popup(ArgWindow, args) {
		ga('send', 'event', 'view', 'text', 'vt_prosm_inf_' +  args.slice(5));
    $('#' + args).modal('open');
    //alert("Test" + args);
}
$('.slider-img').owlCarousel({
    loop: false,
    margin: 10,
    nav: false,
    responsiveClass: true,
    responsive: {
        0: {
            items: 2,
            nav: false
        },
        320: {
            items: 3,
            nav: false
        },
        600: {
            items: 3,
            nav: false
        },
        1000: {
            items: 4,
            nav: false
        }
    }
});
var owl = $(".slider-img").data('owlCarousel');
$('.s-left').click(function(){owl.prev()});
$('.s-right').click(function(){owl.next()});
